# encoding: utf-8
# frozen_string_literal: false

# -*- mode: ruby -*-
# vi: set ft=ruby :

module Guava
  def self.version
    File.read('version').chomp
  end

  def self.release
    File.read('release').chomp
  end

  def self.release_version(release = Guava.release)
    release_version = [Guava.version]
    release_version << release unless release.nil?
    release_version.join('.').freeze
  end

  VERSION = Guava.version
  GEM_RELEASE = Guava.release
end
