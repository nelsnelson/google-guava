#! /usr/bin/env jruby
# vi: set ft=ruby :
# encoding: utf-8

require 'java'
require 'guava'

module Example
  java_import java.util.concurrent.Callable
  java_import java.util.concurrent.Executors
  java_import com.google.common.util.concurrent.FutureCallback
  java_import com.google.common.util.concurrent.Futures
  java_import com.google.common.util.concurrent.MoreExecutors

  class FutureCallbackExample
    include FutureCallback
    def on_success(object)
      puts "Success"
    end
    def on_failure(throwable)
      if throwable.respond_to? :message
        puts "Failure message: #{throwable.message}"
      else
        puts "Failure: #{throwable}"
      end
    end
  end
  class CallableExample
    include Callable
    def call
      begin
        a = 10 / 0
        'done'
      rescue StandardError => e
        raise e
      end
    end
  end

  def main(args=ARGV)
    executor = Executors.newCachedThreadPool()
    executor = MoreExecutors::listeningDecorator(executor)
    callable = CallableExample.new
    listenable_future = executor.submit(callable)
    future_callback = FutureCallbackExample.new
    Futures.addCallback(listenable_future, future_callback, executor)
    executor.shutdown()
    while not executor.isTerminated()
      sleep 1
    end
    exit
  end
end

Object.new.extend(Example).main if $PROGRAM_NAME == __FILE__

