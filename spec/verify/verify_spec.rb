# encoding: utf-8

RSpec.describe 'verify gem' do
  it 'should support failed callable detection' do
    require 'fileutils'
    gem_path = ENV['GEM_PATH']
    gem_spec_full_name = ENV['GEM_SPEC_FULL_NAME']
    FileUtils.mkdir_p 'tmp'
    system('jgem', 'install', '--install-dir=tmp', gem_path)
    gem_lib_dir_path = File.join('tmp', 'gems', gem_spec_full_name, 'lib')
    verify_rb_file_path = File.join('spec', 'verify', 'example.rb')
    begin
      cmd = "jruby -I#{gem_lib_dir_path} #{verify_rb_file_path}"
      results = `#{cmd}`.strip
      expected_results = "Failure message: (ZeroDivisionError) divided by 0"
      expect(results).to eq(expected_results)
    rescue StandardError => e
      STDERR.puts "Unexpected error: #{e.message}"
    end
  end
end

