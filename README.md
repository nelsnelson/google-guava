# google-guava

This packages the Google Guava jars into a convenient gem for requiring.

```
require 'guava'
```

## Building

To clean the project, run unit tests, build the gem file, and verify that the built artifact works, execute:

```{sh}
jruby -S rake clobber package spec gem verify
```

## Publish

To publish the gem, execute:

```{sh}
jruby -S rake publish
```

